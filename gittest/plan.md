# Study Plan for Johnny English

* This is my stuy plan for the first semester:

| Module | Credit points (cp, LP) | Semester | exam in every semester?|
| ------- | --------------- | -------------- | ---------------------- |
| Einführung in die Mathematik für Informatiker	| 15 | 1 |yes|
| Algorithmen und Datenstrukturen	| 6 | 1 |yes|
| Einführungspraktikum RoboLab	| 4 | 1 |yes|
| Einführung in die Medieninformatik	| 5 | 1 |yes|
| Mathematische Methoden für Informatiker	 (Teil A) | 7.5 | 2 |yes|
| Programmierung	 	| 6 | 2 |yes|
| Softwaretechnologie	 	| 6 | 2 |yes|
| Informations- und Kodierungstheorie	 	| 5 | 2 |yes|
